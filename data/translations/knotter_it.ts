<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>About_Dialog</name>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="80"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="253"/>
        <source>License</source>
        <translation>Licenza</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="86"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2012-2020 Mattia Basaglia&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is free software: you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Free Software Foundation, either version 3 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with this program.  If not, see &amp;lt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&amp;gt;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="115"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="258"/>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="151"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="254"/>
        <source>Data directories:</source>
        <translation>Cartelle con i dati:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="165"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="255"/>
        <source>Settings file:</source>
        <translation>File di configurazione:</translation>
    </message>
    <message>
        <source>Settings location:</source>
        <translation type="obsolete">File di configurazione:</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="obsolete">Plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="182"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="256"/>
        <source>Plugins are loaded from the following directories:</source>
        <translation>I plugin vengono caricati dalle seguenti cartelle:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="196"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="257"/>
        <source>Writable data directory:</source>
        <translation>Cartella dati utente:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.ui" line="223"/>
        <location filename="../../src/generated/ui_about_dialog.h" line="259"/>
        <source>About Qt</source>
        <translation>Informazioni su Qt</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="42"/>
        <location filename="../../src/dialogs/about_dialog.cpp" line="65"/>
        <source>About %1</source>
        <translation>Informazioni su %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="103"/>
        <source>Copy directory name</source>
        <translation>Copia nome cartella</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="104"/>
        <source>Open in file manager</source>
        <translation>Apri nel gestore dei file</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="111"/>
        <source>Copy file name</source>
        <translation>Copia nome file</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/about_dialog.cpp" line="112"/>
        <source>Open file</source>
        <translation>Apri file</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_about_dialog.h" line="231"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Copyright (C) 2012-2014  Mattia Basaglia&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is free software: you can redistribute it and/or modify&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;it under the terms of the GNU General Public License as published by&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;the Free Software Foundation, either version 3 of the License, or&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;(at your option) any later version.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Knotter is distributed in the hope that it will be useful,&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;GNU General Public License for more details.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;You should have received a copy of the GNU General Public License&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;along with this program.  If not, see &amp;lt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&amp;gt;.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AbstractWidgetList</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="67"/>
        <source>Add New</source>
        <translation type="unfinished">Aggiungi Nuovo</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="118"/>
        <source>Move Up</source>
        <translation type="unfinished">Muovi Su</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="119"/>
        <source>Move Down</source>
        <translation type="unfinished">Muovi Giù</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/abstract_widget_list.cpp" line="120"/>
        <source>Remove</source>
        <translation type="unfinished">Rimuovi</translation>
    </message>
</context>
<context>
    <name>Abstract_Widget_List</name>
    <message>
        <source>Add New</source>
        <translation type="vanished">Aggiungi Nuovo</translation>
    </message>
    <message>
        <source>Move Up</source>
        <translation type="vanished">Muovi Su</translation>
    </message>
    <message>
        <source>Move Down</source>
        <translation type="vanished">Muovi Giù</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Rimuovi</translation>
    </message>
</context>
<context>
    <name>Application_Info</name>
    <message>
        <location filename="../../src/application_info.cpp" line="41"/>
        <source>Knotter</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Brush_Style</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="448"/>
        <source>Change Brush Style</source>
        <translation>Cambia Motivo</translation>
    </message>
</context>
<context>
    <name>Change_Borders</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="228"/>
        <source>Change Border</source>
        <translation>Cambia Bordo</translation>
    </message>
</context>
<context>
    <name>Change_Colors</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="191"/>
        <source>Change Color</source>
        <translation>Cambia Colore</translation>
    </message>
</context>
<context>
    <name>Change_Edge_Style</name>
    <message>
        <source>Change Edge Type</source>
        <translation type="obsolete">Cambia tipo di Arco</translation>
    </message>
</context>
<context>
    <name>Change_Edge_Type</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="721"/>
        <source>Change Edge Type</source>
        <translation>Cambia tipo di Arco</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="14"/>
        <source>Select Color</source>
        <translation type="unfinished">Seleziona Colore</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="60"/>
        <source>Saturation</source>
        <translation type="unfinished">Saturazione</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="67"/>
        <source>Hue</source>
        <translation type="unfinished">Tonalità</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="84"/>
        <source>Hex</source>
        <translation type="unfinished">Esadecimale</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="91"/>
        <source>Blue</source>
        <translation type="unfinished">Blu</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="128"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="135"/>
        <source>Green</source>
        <translation type="unfinished">Verde</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="142"/>
        <source>Alpha</source>
        <translation type="unfinished">Trasparenza</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.ui" line="149"/>
        <source>Red</source>
        <translation type="unfinished">Rosso</translation>
    </message>
</context>
<context>
    <name>Color_Dialog</name>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="318"/>
        <source>Select Color</source>
        <translation>Seleziona Colore</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="319"/>
        <source>Saturation</source>
        <translation>Saturazione</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="320"/>
        <source>Hue</source>
        <translation>Tonalità</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="321"/>
        <source>Hex</source>
        <translation>Esadecimale</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="322"/>
        <source>Blue</source>
        <translation>Blu</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="323"/>
        <source>Value</source>
        <translation>Luminosità</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="324"/>
        <source>Green</source>
        <translation>Verde</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="325"/>
        <source>Alpha</source>
        <translation>Trasparenza</translation>
    </message>
    <message>
        <location filename="../../src/generated/ui_color_dialog.h" line="326"/>
        <source>Red</source>
        <translation>Rosso</translation>
    </message>
    <message>
        <source>Pick</source>
        <translation type="vanished">Seleziona</translation>
    </message>
</context>
<context>
    <name>Context_Menu_Edge</name>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="36"/>
        <source>Snap to grid</source>
        <translation>Blocca sulla griglia</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="37"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="39"/>
        <source>Edge type</source>
        <translation>Tipo di Arco</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="43"/>
        <source>Break on intersections</source>
        <translation>Spezza nelle intersezioni</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="44"/>
        <source>Subdivide...</source>
        <translation>Suddividi...</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="46"/>
        <source>Properties...</source>
        <translation>Proprietà...</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="47"/>
        <source>Reset custom style</source>
        <translation>Reimposta stile</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="51"/>
        <source>Edge Context Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="80"/>
        <source>Snap to Grid</source>
        <translation>Blocca sulla griglia</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="176"/>
        <source>Break edge</source>
        <translation>Spezza arco</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="177"/>
        <source>Number of segments</source>
        <translation>Numero di segmenti</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="181"/>
        <source>Subdivide Edge</source>
        <translation>Suddividi arco</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_edge.cpp" line="225"/>
        <source>Reset Edge Style</source>
        <translation>Reimposta stile arco</translation>
    </message>
</context>
<context>
    <name>Context_Menu_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="35"/>
        <source>Snap to grid</source>
        <translation>Blocca sulla griglia</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="36"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="37"/>
        <source>Properties...</source>
        <translation>Proprietà...</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="38"/>
        <source>Reset custom style</source>
        <translation>Reimposta stile</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="40"/>
        <source>Node Context Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="56"/>
        <source>Reset Node Style</source>
        <translation>Reimposta stile</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/context_menu_node.cpp" line="79"/>
        <source>Snap to Grid</source>
        <translation>Blocca sulla griglia</translation>
    </message>
</context>
<context>
    <name>Create_Edge</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="113"/>
        <source>Create Edge</source>
        <translation>Crea Arco</translation>
    </message>
</context>
<context>
    <name>Create_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="82"/>
        <source>Create Node</source>
        <translation>Crea Vertice</translation>
    </message>
</context>
<context>
    <name>Crossing_Style_Widget</name>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="33"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="139"/>
        <source>Curve</source>
        <translation>Curva</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="46"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="141"/>
        <source>Size of the curve control handles</source>
        <translation>Dimensione delle maniglie di controllo della curva</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="49"/>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="78"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="143"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="148"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="65"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="144"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="75"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="146"/>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation>Distanza tra le estremità di un filo che passa sotto ad un altro</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="91"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="149"/>
        <source>Gap</source>
        <translation>Spazio</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="117"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="151"/>
        <source>Slide</source>
        <translation>Scorri</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/crossing_style_widget.ui" line="127"/>
        <location filename="../../src/generated/ui_crossing_style_widget.h" line="153"/>
        <source>Position of the crossing in the edge</source>
        <translation>Posizione dell&apos;incrocio nell&apos;arco</translation>
    </message>
</context>
<context>
    <name>Cusp_Style_Dialog</name>
    <message>
        <location filename="../../src/dialogs/cusp_style_dialog.ui" line="14"/>
        <location filename="../../src/generated/ui_cusp_style_dialog.h" line="66"/>
        <source>Node Preferences</source>
        <translation>Impostazioni Vertice</translation>
    </message>
</context>
<context>
    <name>Cusp_Style_Widget</name>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="55"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="152"/>
        <source>Curve</source>
        <translation>Curva</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="103"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="161"/>
        <source>Size of the curve control handles</source>
        <translation>Dimensione delle maniglie di controllo della curva</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="84"/>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="106"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="159"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="163"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <source>Gap</source>
        <translation type="obsolete">Spazio</translation>
    </message>
    <message>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation type="obsolete">Distanza tra le estremità di un filo che passa sotto ad un altro</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="39"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="151"/>
        <source>Angle</source>
        <translation>Angolo</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="20"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="148"/>
        <source>Minimum angle required to render a cusp</source>
        <translation>Minimo angolo necessario per disegnare una cuspide</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="26"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="150"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="119"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="164"/>
        <source>Shape</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="145"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="165"/>
        <source>Distance</source>
        <translation>Distanza</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="81"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="157"/>
        <source>Distance of the cusp tip from the node position</source>
        <translation>Distanza tra la punta della cuspine e la posizione del vertice</translation>
    </message>
    <message>
        <source>Style</source>
        <translation type="obsolete">Stile</translation>
    </message>
    <message>
        <location filename="../../src/widgets/other_widgets/cusp_style_widget.ui" line="68"/>
        <location filename="../../src/generated/ui_cusp_style_widget.h" line="154"/>
        <source>Shape style</source>
        <translation>Forma della cuspide</translation>
    </message>
</context>
<context>
    <name>Custom_Colors</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="265"/>
        <source>Custom Colors</source>
        <translation>Colori personalizzati</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="267"/>
        <source>Auto Color</source>
        <translation>Colori automatici</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <source>Concentric Frames</source>
        <translation type="vanished">Cornici contentriche</translation>
    </message>
    <message>
        <source>Width</source>
        <translation type="vanished">Largezza</translation>
    </message>
    <message>
        <source>Iterations</source>
        <translation type="vanished">Iterazioni</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation type="vanished">Connesse</translation>
    </message>
    <message>
        <source>Delete original selection</source>
        <translation type="vanished">Cancella la selezione originale</translation>
    </message>
    <message>
        <source>Configure external commands</source>
        <translation type="vanished">Configura programmi esterni</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">Nome</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="vanished">Esporta</translation>
    </message>
    <message>
        <source>Executable</source>
        <translation type="vanished">Eseguibile</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="vanished">Rimuovi</translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">Aggiungi</translation>
    </message>
    <message>
        <source>Fill Area</source>
        <translation type="vanished">Riempi area</translation>
    </message>
    <message>
        <source>Grid Size</source>
        <translation type="vanished">Griglia</translation>
    </message>
    <message>
        <source>Polygon</source>
        <translation type="vanished">Poligono</translation>
    </message>
    <message>
        <source>Convex Hull</source>
        <translation type="vanished">Inviluppo convesso</translation>
    </message>
    <message>
        <source>Single Edge Loop</source>
        <translation type="vanished">Singola lista di archi</translation>
    </message>
    <message>
        <source>Outline</source>
        <translation type="vanished">Bordo</translation>
    </message>
    <message>
        <source>Thikness</source>
        <translation type="vanished">Spessore</translation>
    </message>
    <message>
        <source>Outset</source>
        <translation type="vanished">Circoscritto</translation>
    </message>
    <message>
        <source>Inset</source>
        <translation type="vanished">Inscritto</translation>
    </message>
    <message>
        <source>Insert Lattice</source>
        <translation type="vanished">Inserisci reticolo</translation>
    </message>
    <message>
        <source>Rows</source>
        <translation type="vanished">Righe</translation>
    </message>
    <message>
        <source>Columns</source>
        <translation type="vanished">Colonne</translation>
    </message>
    <message>
        <source>Cell size</source>
        <translation type="vanished">Dimensione Cella</translation>
    </message>
    <message>
        <source>Mirror</source>
        <translation type="vanished">Rifletti</translation>
    </message>
    <message>
        <source>Mirror X</source>
        <translation type="vanished">Rifletti lungo l&apos;asse X</translation>
    </message>
    <message>
        <source>Mirror Y</source>
        <translation type="vanished">Rifletti lungo l&apos;asse Y</translation>
    </message>
    <message>
        <source>Spiral</source>
        <translation type="vanished">Spirale</translation>
    </message>
    <message>
        <source>End Angle</source>
        <translation type="vanished">Angolo finale</translation>
    </message>
    <message>
        <source>°</source>
        <translation type="vanished">°</translation>
    </message>
    <message>
        <source>Start Angle</source>
        <translation type="vanished">Angolo Iniziale</translation>
    </message>
    <message>
        <source>Turning Size</source>
        <translation type="vanished">Dimensione curva</translation>
    </message>
    <message>
        <source>Angle Step</source>
        <translation type="vanished">Incremento angolo</translation>
    </message>
</context>
<context>
    <name>Dialog_Confirm_Close</name>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="103"/>
        <source>Close Files</source>
        <translation>Chiudi file</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="20"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="104"/>
        <source>The following files have unsaved changes:</source>
        <translation>I seguenti file hanno modifiche non salvate:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="45"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="105"/>
        <source>Save selected files?</source>
        <translation>Salvare i file selezionati?</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="54"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="106"/>
        <source>&amp;Save Selected</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="64"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="107"/>
        <source>Do&amp;n&apos;t Save</source>
        <translation>&amp;Non Salvare</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_confirm_close.ui" line="74"/>
        <location filename="../../src/generated/ui_dialog_confirm_close.h" line="108"/>
        <source>&amp;Cancel</source>
        <translation>&amp;Annulla</translation>
    </message>
</context>
<context>
    <name>Dialog_Download_Plugin</name>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.ui" line="22"/>
        <location filename="../../src/generated/ui_dialog_download_plugin.h" line="92"/>
        <source>Progress:</source>
        <translation>Avanzamento:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.ui" line="54"/>
        <location filename="../../src/generated/ui_dialog_download_plugin.h" line="94"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.ui" line="59"/>
        <location filename="../../src/generated/ui_dialog_download_plugin.h" line="96"/>
        <source>Progress</source>
        <translation>Stato</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="37"/>
        <source>Installing %1</source>
        <translation>Installazione di %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="51"/>
        <source>Queued</source>
        <translation>In coda</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="106"/>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="157"/>
        <source>Download Error</source>
        <translation>Errore di scaricamento</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="107"/>
        <source>Error during the installation of %1</source>
        <translation>Errore durante l&apos;installazione di %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="113"/>
        <source>Download Complete</source>
        <translation>Installazione completata</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="114"/>
        <source>%1 was installed successfully</source>
        <translation>%1 è stato installato con successo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_download_plugin.cpp" line="164"/>
        <source>Download Successful</source>
        <translation>Scaricato</translation>
    </message>
</context>
<context>
    <name>Dialog_Edge_Properties</name>
    <message>
        <location filename="../../src/dialogs/dialog_edge_properties.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_edge_properties.h" line="60"/>
        <source>Edge Properties</source>
        <translation>Poprietà arco</translation>
    </message>
</context>
<context>
    <name>Dialog_Insert_Polygon</name>
    <message>
        <source>Insert Polygon</source>
        <translation type="vanished">Inserisci poligono</translation>
    </message>
    <message>
        <source>Sides</source>
        <translation type="vanished">Lati</translation>
    </message>
    <message>
        <source>Whether there should be a node connected to the vertices on the center of the polygon</source>
        <translation type="vanished">Se ci deve essere un nodo al centro connesso ai vertici del poligono</translation>
    </message>
    <message>
        <source>Node at Center</source>
        <translation type="vanished">Nodo al centro</translation>
    </message>
</context>
<context>
    <name>Dialog_Insert_Star</name>
    <message>
        <source>Insert Star</source>
        <translation type="vanished">Inserisci lati</translation>
    </message>
    <message>
        <source>Sides</source>
        <translation type="vanished">Lati</translation>
    </message>
    <message>
        <source>Inner Radius</source>
        <translation type="vanished">Raggio interno</translation>
    </message>
    <message>
        <source>Outer Radius</source>
        <translation type="vanished">Raggio esterno</translation>
    </message>
</context>
<context>
    <name>Dialog_Plugins</name>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="486"/>
        <source>Plugins</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <source>Enable</source>
        <translation type="obsolete">Abilita</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="34"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="500"/>
        <source>Installed</source>
        <translation>Installati</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="132"/>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="213"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="487"/>
        <source>Disabled</source>
        <translation>Disabilita</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="142"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="488"/>
        <source>Reload Script</source>
        <translation>Ricarica script</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="154"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="489"/>
        <source>Edit Script</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="175"/>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="385"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="490"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="503"/>
        <source>Plugin data</source>
        <translation>Dati del Plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="192"/>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="411"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="492"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="505"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="197"/>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="416"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="494"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="507"/>
        <source>Value</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="207"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="495"/>
        <source>View &amp;Settings...</source>
        <translation>Visualizza Impo&amp;stazioni...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="219"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="496"/>
        <source>C&amp;lear Settings</source>
        <translation>Cance&amp;lla Impostazioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="239"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="497"/>
        <source>No plugin installed</source>
        <translation>Nessun plugin installato</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="248"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="498"/>
        <source>&amp;Reload Plugins</source>
        <translation>&amp;Ricarica i Plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="260"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="499"/>
        <source>&amp;Create...</source>
        <translation>&amp;Crea...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="280"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="510"/>
        <source>Browse Online</source>
        <translation>Disponibili on-line</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="316"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="501"/>
        <source>&amp;Refresh List</source>
        <translation>Aggio&amp;rna lista</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="366"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="502"/>
        <source>&amp;Install</source>
        <translation>&amp;Installa</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="424"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="508"/>
        <source>Files</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.ui" line="467"/>
        <location filename="../../src/generated/ui_dialog_plugins.h" line="509"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>&amp;Install...</source>
        <translation type="obsolete">&amp;Installa...</translation>
    </message>
    <message>
        <source>&amp;OK</source>
        <translation type="obsolete">&amp;OK</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="88"/>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="331"/>
        <source>All</source>
        <translation>Tutti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="213"/>
        <source>Enabled</source>
        <translation>Abilitato</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="251"/>
        <source>Connecting...</source>
        <translation>Connessione in corso...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="271"/>
        <source>Finished</source>
        <translation>Finito</translation>
    </message>
    <message>
        <source>Error %1</source>
        <translation type="vanished">Errore %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="276"/>
        <source>Error %1: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="350"/>
        <source>Loading...</source>
        <translation>Caricamento...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="372"/>
        <source>No plugin loaded</source>
        <translation>Nessun plugin caricato</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="386"/>
        <source>You don&apos;t have this plugin</source>
        <translation>Non hai questo plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="395"/>
        <source>You already have this plugin</source>
        <translation>Hai già questo plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_plugins.cpp" line="399"/>
        <source>You can upgrade this plugin</source>
        <translation>Puoi aggiornare questo plugin</translation>
    </message>
</context>
<context>
    <name>Dialog_Preferences</name>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="14"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="686"/>
        <source>Preferences</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="125"/>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="194"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="693"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="706"/>
        <source>Toolbars</source>
        <translation>Barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="112"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="691"/>
        <source>General</source>
        <translation>Generale</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="138"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="695"/>
        <source>Performance</source>
        <translation>Prestazioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="393"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="730"/>
        <source>Icon Size</source>
        <translation>Dimensione Icone</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="400"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="732"/>
        <source>Icon size</source>
        <translation>Dimensione Icone</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="403"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="735"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Select the size used for toolbar icons and items in the drawing area.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Seleziona la dimensione usata dalle icone nelle barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="410"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="737"/>
        <source>Toolbar Button Style</source>
        <translation>Stile pulsanti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="417"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="739"/>
        <source>Select how the toolbar should display icons and text</source>
        <translation>Selezione come i pulsanti vengono visualizzati nelle barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="424"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="741"/>
        <source>Toolbar Buttons</source>
        <translation>Pulsanti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="191"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="704"/>
        <source>Save the customized toolbar buttons</source>
        <translation>Salva i pultanti personalizzati delle barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="204"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="708"/>
        <source>Dialogs, window size and position</source>
        <translation>Dialoghi, dimensione e posizione della finestra</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="217"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="711"/>
        <source>Knot Style</source>
        <translation>Sile Nodo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="266"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="719"/>
        <source>Max Recent Files</source>
        <translation>Massimo numero di documenti recenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="256"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="718"/>
        <source>Check for unsaved files on close</source>
        <translation>Controlla file non salvati durante la chiusura</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="234"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="714"/>
        <source>Clear recent file history</source>
        <translation>Cancella storico dei documenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="237"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="716"/>
        <source>Clear</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="249"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="717"/>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="185"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="702"/>
        <source>Save between sessions</source>
        <translation>Salva tra le sessioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="148"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="697"/>
        <source>Interface</source>
        <translation>Interfaccia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="207"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="710"/>
        <source>User Interface</source>
        <translation>Interfaccia Grafica</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="224"/>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="681"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="712"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="783"/>
        <source>Grid</source>
        <translation>Griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="374"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="727"/>
        <source>Clear configuration and restore defaults next time Knotter is launched</source>
        <translation>Cancella impostazioni e reimposta quelle predefinite</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="377"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="729"/>
        <source>Clear  saved configuration</source>
        <translation>Cancella impostazioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="289"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="720"/>
        <source>Clipboard</source>
        <translation>Appunti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="158"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="699"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="304"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="721"/>
        <source>Knot</source>
        <translation>Nodo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="314"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="722"/>
        <source>XML</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="329"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="723"/>
        <source>SVG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="344"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="724"/>
        <source>PNG</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="359"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="725"/>
        <source>TIFF</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="440"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="742"/>
        <source>Rendering</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="446"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="744"/>
        <source>Enable caching of the rendered knot image</source>
        <translation>Abilita la cache dell&apos;immagine processata del nodo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="449"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="747"/>
        <source>Enable caching of the rendered knot image. Imporves performance in large scenes but may create some artifacts.</source>
        <translation>Abilita la cache dell&apos;immagine processata del nodo. Migliora le prestazioni in scene molto grandi ma può creare alcune distorsioni.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="452"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="749"/>
        <source>Cache image</source>
        <translation>Cache immagine</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="459"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="751"/>
        <source>Redraw the knot while moving nodes</source>
        <translation>Ridisegna il nodo quando vengono spostati i vertici</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="462"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="754"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When selected the knot will be updated fluidly when moving nodes.&lt;/p&gt;&lt;p&gt;When disabled, will update only after the nodes have been moved.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>Quando attivato, il nodo verrà aggiornato in modo fluido durante lo spostamento dei vertici. Se disabilitato, il nodo sarà aggiornanot solo dopo che lo spostamento è stato completato</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="465"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="756"/>
        <source>Fluid refresh</source>
        <translation>Aggiornamento fluido</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="475"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="757"/>
        <source>Antialiasing</source>
        <translation>Antialias</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="485"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="758"/>
        <source>Script</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="491"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="759"/>
        <source>Timeout</source>
        <translation>Tempo massimo di esecuzione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="498"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="761"/>
        <source>Maximum execution time for scripts in seconds, 0 means no limit.</source>
        <translation>Tempo massimo di esecuzione in secondi, 0 significa illimitato.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="501"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="763"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="531"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="764"/>
        <source>Node Icon Size</source>
        <translation>Dimensione icona nodi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="538"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="765"/>
        <source>Widget Style</source>
        <translation>Stile interfaccia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="548"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="767"/>
        <source>User interface style. Some widgets may require a restart to take effect.</source>
        <translation>Stile dell&apos;interfaccia. Alcuni elementi potrebbero richiedere un riavvio per effettuare il cambiamento.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="555"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="770"/>
        <source>Shows a preview of the widget style</source>
        <translation>Mostra un&apos;anteprima dello stile dell&apos;interfaccia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="558"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="772"/>
        <source>Preview</source>
        <translation>Anteprima</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="564"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="773"/>
        <source>Button</source>
        <translation>Pulsante</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="575"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="774"/>
        <source>Item1</source>
        <translation>Oggetto1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="580"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="775"/>
        <source>Item2</source>
        <translation>Oggetto2</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="585"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="776"/>
        <source>Item3</source>
        <translation>Oggetto3</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="596"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="778"/>
        <source>Colors</source>
        <translation>Colori</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="635"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="779"/>
        <source>Edge (Highlighted)</source>
        <translation>Arco (Evidenziato)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="651"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="780"/>
        <source>Edge (Selected)</source>
        <translation>Arco (Selezionato)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="664"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="781"/>
        <source>Edge (Resting)</source>
        <translation>Arco (Normale)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="671"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="782"/>
        <source>Node (Resting)</source>
        <translation>Nodo (Normale)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="691"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="784"/>
        <source>Node (Selected)</source>
        <translation>Nodo (Selezionato)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="698"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="785"/>
        <source>Node (Highlighted)</source>
        <translation>Nodo (Evidenziato)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="744"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="786"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="754"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="787"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.ui" line="764"/>
        <location filename="../../src/generated/ui_dialog_preferences.h" line="788"/>
        <source>Cancel</source>
        <translation type="unfinished">Annulla</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="99"/>
        <source>Small (16x16)</source>
        <translation>Piccole (16x16)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="100"/>
        <source>Medium (22x22)</source>
        <translation>Medie (22x22)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="101"/>
        <source>Large (48x48)</source>
        <translation>Grandi (48x48)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="102"/>
        <source>Huge (64x64)</source>
        <translation>Enormi (64x64)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="108"/>
        <source>Icon Only</source>
        <translation>Solo Icone</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="109"/>
        <source>Text Only</source>
        <translation>Solo Testo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="110"/>
        <source>Text Beside Icon</source>
        <translation>Testo in parte all&apos;icona</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="111"/>
        <source>Text Under Icon</source>
        <translation>Testo sotto all&apos;icona</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="112"/>
        <source>Follow System Style</source>
        <translation>Segui stile di sistema</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="119"/>
        <source>Small</source>
        <translation>Piccola</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="120"/>
        <source>Medium</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="121"/>
        <source>Large</source>
        <translation>Grande</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="191"/>
        <source>Clearing Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dialog_preferences.cpp" line="192"/>
        <source>Next time %1 will start with the default settings.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Display_Border</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="287"/>
        <source>Display Borders</source>
        <translation>Mostra bordi</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="289"/>
        <source>Hide Borders</source>
        <translation>Nascondi bordi</translation>
    </message>
</context>
<context>
    <name>Dock_Background</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="17"/>
        <location filename="../../src/generated/ui_dock_background.h" line="152"/>
        <source>Background</source>
        <translation>Sfondo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="42"/>
        <location filename="../../src/generated/ui_dock_background.h" line="153"/>
        <source>Color</source>
        <translation>Colore</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="72"/>
        <location filename="../../src/generated/ui_dock_background.h" line="154"/>
        <source>Image</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="84"/>
        <location filename="../../src/generated/ui_dock_background.h" line="155"/>
        <source>File</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="94"/>
        <location filename="../../src/generated/ui_dock_background.h" line="156"/>
        <source>Browse...</source>
        <translation>Sfoglia...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="106"/>
        <location filename="../../src/generated/ui_dock_background.h" line="157"/>
        <source>Scale</source>
        <translation>Dimensione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="113"/>
        <location filename="../../src/generated/ui_dock_background.h" line="158"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.ui" line="129"/>
        <location filename="../../src/generated/ui_dock_background.h" line="159"/>
        <source>Move...</source>
        <translation>Muovi...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.cpp" line="68"/>
        <source>Open background image</source>
        <translation>Apri immagine di sfondo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.cpp" line="70"/>
        <source>All supported images (%1)</source>
        <translation>Tutte le immagini supportate (%1)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_background.cpp" line="70"/>
        <source>All files (*)</source>
        <translation>Tutti i file (*)</translation>
    </message>
</context>
<context>
    <name>Dock_Borders</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_borders.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_borders.h" line="69"/>
        <source>Border</source>
        <translation>Bordo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_borders.ui" line="26"/>
        <location filename="../../src/generated/ui_dock_borders.h" line="70"/>
        <source>Enable Borders</source>
        <translation>Abilita Bordi</translation>
    </message>
</context>
<context>
    <name>Dock_Grid</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="197"/>
        <source>Configure Grid</source>
        <translation>Configura Griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="50"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="198"/>
        <source>Enable Grid</source>
        <translation>Abilita Griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="65"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="199"/>
        <source>Size</source>
        <translation>Dimensione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="72"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="200"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="119"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="205"/>
        <source>Shape</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="86"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="201"/>
        <source>Square</source>
        <translation>Quadrato</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="96"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="202"/>
        <source>Triangle 1</source>
        <translation>Triangolo 1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="106"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="203"/>
        <source>Triangle 2</source>
        <translation>Triangolo 2</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="128"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="206"/>
        <source>Origin</source>
        <translation>Origine</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="183"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="212"/>
        <source>Reset</source>
        <translation>Reimposta</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="140"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="207"/>
        <source>x</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="180"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="210"/>
        <source>Reset grid position</source>
        <translation>Reimposta la posizione della griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="190"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="213"/>
        <source>Move...</source>
        <translation>Muovi...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_grid.ui" line="163"/>
        <location filename="../../src/generated/ui_dock_grid.h" line="208"/>
        <source>y</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Dock_Knot_Display</name>
    <message>
        <source>Knot Display</source>
        <translation type="obsolete">Visione nodo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="17"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="142"/>
        <source>Appearance</source>
        <translation>Aspetto</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="120"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="151"/>
        <source>Custom Colors</source>
        <translation>Colori personalizzati</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="79"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="147"/>
        <source>Width</source>
        <translation>Largezza</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="86"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="148"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="113"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="150"/>
        <source>Joint Style</source>
        <translation>Sile punte</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="46"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="143"/>
        <source>Bevel</source>
        <translation>Smussata</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="56"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="144"/>
        <source>Miter</source>
        <translation>Appuntita</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="66"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="145"/>
        <source>Round</source>
        <translation>Arrotondata</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_display.ui" line="106"/>
        <location filename="../../src/generated/ui_dock_knot_display.h" line="149"/>
        <source>Pattern</source>
        <translation>Motivo</translation>
    </message>
</context>
<context>
    <name>Dock_Knot_Style</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="192"/>
        <source>Default Style</source>
        <translation>Sile predefinito</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="56"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="193"/>
        <source>Curve</source>
        <translation>Curva</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="66"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="195"/>
        <source>Size of the curve control handles</source>
        <translation>Dimensione delle maniglie di controllo della curva</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="69"/>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="142"/>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="209"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="197"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="208"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="218"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="82"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="198"/>
        <source>Nodes</source>
        <translation>Nodi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="100"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="199"/>
        <source>Angle</source>
        <translation>Angolo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="110"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="201"/>
        <source>Minimum angle required to render a cusp</source>
        <translation>Minimo angolo necessario per disegnare una cuspide</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="116"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="203"/>
        <source>°</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="129"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="204"/>
        <source>Distance</source>
        <translation>Distanza</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="139"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="206"/>
        <source>Distance of the cusp tip from the node position</source>
        <translation>Distanza tra la punta della cuspine e la posizione del vertice</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="158"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="209"/>
        <source>Shape</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="168"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="211"/>
        <source>Shape style</source>
        <translation>Forma della cuspide</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="178"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="213"/>
        <source>Edges</source>
        <translation>Archi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="196"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="214"/>
        <source>Gap</source>
        <translation>Spazio</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="206"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="216"/>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation>Distanza tra le estremità di un filo che passa sotto ad un altro</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="228"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="219"/>
        <source>Slide</source>
        <translation>Scorri</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_knot_style.ui" line="238"/>
        <location filename="../../src/generated/ui_dock_knot_style.h" line="221"/>
        <source>Position of the crossing in the edge</source>
        <translation>Posizione dell&apos;incrocio nell&apos;arco</translation>
    </message>
</context>
<context>
    <name>Dock_Script_Log</name>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="19"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="364"/>
        <source>Script Console</source>
        <translation>Terminale Script</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="72"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="365"/>
        <source>&amp;New</source>
        <translation>&amp;Nuovo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="87"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="366"/>
        <source>&amp;Open</source>
        <translation>A&amp;pri</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="99"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="367"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="111"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="368"/>
        <source>Save &amp;As</source>
        <translation>S&amp;alva come</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="133"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="369"/>
        <source>&amp;Undo</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="148"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="370"/>
        <source>R&amp;edo</source>
        <translation>R&amp;ipeti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="167"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="372"/>
        <source>External Editor</source>
        <translation>Editor Interno</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="170"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="374"/>
        <source>E&amp;xternal Editor</source>
        <translation>Editor E&amp;sterno</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="182"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="376"/>
        <source>If enabled, this dialog won&apos;t be dockable</source>
        <translation>Se abilitato, questo dialogo non sarà attaccabile alla finestra principale</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="185"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="378"/>
        <source>S&amp;tand Alone Dialog</source>
        <translation>Dialogo a sè s&amp;tant</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="207"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="380"/>
        <source>Deploy Plugin</source>
        <translation>Apllica plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="210"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="382"/>
        <source>&amp;Deploy Plugin</source>
        <translation>A&amp;pplica Plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="252"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="383"/>
        <source>Run</source>
        <translation>Esegui</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="290"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="384"/>
        <source>&amp;Run</source>
        <translation>&amp;Esegui</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="302"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="385"/>
        <source>Clear Output</source>
        <translation>Cancella output</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="317"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="387"/>
        <source>Stop the currently running script</source>
        <translation>Termina l&apos;esecuione dello script corrente</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.ui" line="320"/>
        <location filename="../../src/generated/ui_dock_script_log.h" line="389"/>
        <source>Abort Scripts</source>
        <translation>Termina script</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="140"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="209"/>
        <source>Script Editor</source>
        <translation>Editor di script</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="249"/>
        <source>Open Script</source>
        <translation>Apri Script</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/dock/dock_script_log.cpp" line="270"/>
        <source>Save Script</source>
        <translation>Salva Script</translation>
    </message>
</context>
<context>
    <name>Edge_Style_All</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="860"/>
        <source>Change Edge Style</source>
        <translation>Cambia stile arco</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Crossing_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="679"/>
        <source>Change Selection Crossing Gap</source>
        <translation>Cambia spazio d&apos;incrocio della selezione</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Edge_Slide</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="698"/>
        <source>Slide Selected Edges</source>
        <translation>Scorri archi selezionati</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Enable</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="815"/>
        <source>Toggle Selection Style Property</source>
        <translation>Cambia proprietà di stile</translation>
    </message>
</context>
<context>
    <name>Edge_Style_Handle_Lenght</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="759"/>
        <source>Change Edge Curve Control</source>
        <translation>Cambia controllo curve dell&apos;arco</translation>
    </message>
</context>
<context>
    <name>Export_Image_Dialog</name>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="14"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="247"/>
        <source>Export Image</source>
        <translation>Esporta Immagine</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="25"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="248"/>
        <source>Include Graph in output</source>
        <translation>Ingludi grafo nell&apos;output</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="32"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="249"/>
        <source>Include background image</source>
        <translation>Includi immagine di sfondo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="45"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="250"/>
        <source>Vector</source>
        <translation>Vettoriale</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="51"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="251"/>
        <source>Export &amp;SVG...</source>
        <translation>Esporta &amp;SVG...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="72"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="252"/>
        <source>Raster</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="78"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="254"/>
        <source>Avoid jagged edges</source>
        <translation>Evita i bordi frastagliati</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="81"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="256"/>
        <source>Antialiasing</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="97"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="257"/>
        <source>Compression</source>
        <translation>Compressione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="106"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="259"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;On some formats, higher compression means less quality.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>In alcuni formati, un livello alto di compressione significa qualità peggiore dell&apos;immagine</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="137"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="261"/>
        <source>Width</source>
        <translation>Largezza</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="144"/>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="164"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="262"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="264"/>
        <source>px</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="157"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="263"/>
        <source>Height</source>
        <translation>Altezza</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="177"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="265"/>
        <source>R&amp;eset Size</source>
        <translation>R&amp;eimposta</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="184"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="266"/>
        <source>Export &amp;Image...</source>
        <translation>Esporta &amp;Immagine...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="202"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="268"/>
        <source>Keep Ratio</source>
        <translation>Mantieni proporzioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="205"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="270"/>
        <source>Keep &amp;Ratio</source>
        <translation>Mantieni p&amp;roporzioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.ui" line="223"/>
        <location filename="../../src/generated/ui_export_image_dialog.h" line="271"/>
        <source>Background</source>
        <translation>Sfondo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="80"/>
        <source>%1%</source>
        <extracomment>Compression percentage</extracomment>
        <translation>%1%</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="86"/>
        <source>Export Knot as SVG</source>
        <translation>Esporta come SVG</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="111"/>
        <source>File Error</source>
        <translation>Errore di file</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="111"/>
        <source>Could not write to &quot;%1&quot;.</source>
        <translation>Impossibile scrivere su %1.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="125"/>
        <source>PNG Images (*.png)</source>
        <translation>Immagini PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="126"/>
        <source>Jpeg Images (*.jpg *.jpeg)</source>
        <translation>Immagini Jpeg (*.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="127"/>
        <source>Bitmap (*.bmp)</source>
        <translation>Bitmap (*.bmp)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="128"/>
        <source>All supported images (%1)</source>
        <translation>Tutte le immagini supportate (%1)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="129"/>
        <source>All files (*)</source>
        <translation>Tutti i file (*)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/export_image_dialog.cpp" line="133"/>
        <source>Export Knot as Image</source>
        <translation>Esporta nodo come Immagine</translation>
    </message>
</context>
<context>
    <name>KeySequence_ListWidget</name>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="66"/>
        <source>Clear</source>
        <translation type="unfinished">Cancella</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="67"/>
        <source>Clear shortcut for &quot;%1&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="73"/>
        <source>Reset</source>
        <translation type="unfinished">Reimposta</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_listwidget.cpp" line="74"/>
        <source>Reset shortcut for &quot;%1&quot; to the default %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeySequence_Widget</name>
    <message>
        <location filename="../../src/widgets/keysequence_widget/src/keysequence_widget.cpp" line="88"/>
        <source>(empty)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Knot_Style_All</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="678"/>
        <source>Set Knot Style</source>
        <translation>Imposta stile noso</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Crossing_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="387"/>
        <source>Change Crossing Gap</source>
        <translation>Cambia spazio d&apos;incrocio</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Cusp_Angle</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="421"/>
        <source>Change Minimum Cusp Angle</source>
        <translation>Cambia angolo minimo per cuspide</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Cusp_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="439"/>
        <source>Change Cusp Distance</source>
        <translation>Cambia distanza cuspide</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Cusp_Shape</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="544"/>
        <source>Change Cusp Shape</source>
        <translation>Cambia forma cuspide</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Edge_Slide</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="404"/>
        <source>Slide Crossing</source>
        <translation>Scorri incrocio</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Handle_Lenght</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="370"/>
        <source>Change Curve Control</source>
        <translation>Cambia controllo curve</translation>
    </message>
</context>
<context>
    <name>Knot_Style_Widget</name>
    <message>
        <source>Curve</source>
        <translation type="obsolete">Curva</translation>
    </message>
    <message>
        <source>Size of the curve control handles</source>
        <translation type="obsolete">Dimensione delle maniglie di controllo della curva</translation>
    </message>
    <message>
        <source>Nodes</source>
        <translation type="obsolete">Nodi</translation>
    </message>
    <message>
        <source>Angle</source>
        <translation type="obsolete">Angolo</translation>
    </message>
    <message>
        <source>Minimum angle required to render a cusp</source>
        <translation type="obsolete">Minimo angolo necessario per disegnare una cuspide</translation>
    </message>
    <message>
        <source>Distance</source>
        <translation type="obsolete">Distanza</translation>
    </message>
    <message>
        <source>Distance of the cusp tip from the node position</source>
        <translation type="obsolete">Distanza tra la punta della cuspine e la posizione del vertice</translation>
    </message>
    <message>
        <source>Shape</source>
        <translation type="obsolete">Forma</translation>
    </message>
    <message>
        <source>Shape style</source>
        <translation type="obsolete">Forma della cuspide</translation>
    </message>
    <message>
        <source>Edges</source>
        <translation type="obsolete">Archi</translation>
    </message>
    <message>
        <source>Gap</source>
        <translation type="obsolete">Spazio</translation>
    </message>
    <message>
        <source>Distance between the ends of a thread passing under another thread</source>
        <translation type="obsolete">Distanza tra le estremità di un filo che passa sotto ad un altro</translation>
    </message>
    <message>
        <source>Slide</source>
        <translation type="obsolete">Scorri</translation>
    </message>
    <message>
        <source>Position of the crossing in the edge</source>
        <translation type="obsolete">Posizione dell&apos;incrocio nell&apos;arco</translation>
    </message>
</context>
<context>
    <name>Knot_View</name>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="176"/>
        <source>Load File</source>
        <translation>Carica file</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="319"/>
        <source>Change Edge Type</source>
        <translation>Cambia tipo di Arco</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="389"/>
        <source>Horizontal Flip</source>
        <translation>Simmetria orizzontale</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="404"/>
        <source>Vertical Flip</source>
        <translation>Simmetria verticale</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="466"/>
        <source>Break Edge</source>
        <translation>Spezza arco</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="490"/>
        <source>Remove Node</source>
        <translation>Rimuovi vertice</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="168"/>
        <source>Move Ahead</source>
        <translation>Vai avanti</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="170"/>
        <source>Start Chain</source>
        <translation>Incomincia catena</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="172"/>
        <source>Create Node</source>
        <translation>Crea Vertice</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="174"/>
        <source>Add Edge</source>
        <translation>Aggiungi arco</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="1010"/>
        <source>Move Nodes</source>
        <translation>Sposta vertici</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="1024"/>
        <source>Rotate Nodes</source>
        <translation>Ruota Vertici</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_view.cpp" line="1024"/>
        <source>Scale Nodes</source>
        <translation>Ridimensiona</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/knot_tool.cpp" line="131"/>
        <source>Snap to Grid</source>
        <translation>Blocca sulla griglia</translation>
    </message>
</context>
<context>
    <name>Knot_Width</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="333"/>
        <source>Change Stroke Width</source>
        <translation>Cambia Spessore Linea</translation>
    </message>
</context>
<context>
    <name>Main_Window</name>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="50"/>
        <location filename="../../src/generated/ui_main_window.h" line="827"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="54"/>
        <location filename="../../src/generated/ui_main_window.h" line="828"/>
        <source>Open &amp;Recent</source>
        <translation>Documenti &amp;Recenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="82"/>
        <location filename="../../src/generated/ui_main_window.h" line="829"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifica</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="98"/>
        <location filename="../../src/generated/ui_main_window.h" line="830"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="102"/>
        <location filename="../../src/generated/ui_main_window.h" line="831"/>
        <source>&amp;Toolbars</source>
        <translation>&amp;Barre degli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="122"/>
        <location filename="../../src/generated/ui_main_window.h" line="833"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Zoom</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="145"/>
        <location filename="../../src/generated/ui_main_window.h" line="834"/>
        <source>&amp;Nodes</source>
        <translation>V&amp;ertici</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="162"/>
        <location filename="../../src/generated/ui_main_window.h" line="835"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="172"/>
        <location filename="../../src/generated/ui_main_window.h" line="836"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="182"/>
        <location filename="../../src/generated/ui_main_window.h" line="837"/>
        <source>&amp;Plugins</source>
        <translation>&amp;Plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="287"/>
        <location filename="../../src/generated/ui_main_window.h" line="750"/>
        <source>&amp;Open...</source>
        <translation>&amp;Apri...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="297"/>
        <location filename="../../src/generated/ui_main_window.h" line="751"/>
        <source>&amp;New</source>
        <translation>&amp;Nuovo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="307"/>
        <location filename="../../src/generated/ui_main_window.h" line="752"/>
        <source>&amp;Save</source>
        <translation>&amp;Salva</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="317"/>
        <location filename="../../src/generated/ui_main_window.h" line="753"/>
        <source>Save &amp;As...</source>
        <translation>S&amp;alva con Nome...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="327"/>
        <location filename="../../src/generated/ui_main_window.h" line="754"/>
        <source>Save A&amp;ll</source>
        <translation>Sa&amp;lva Tutti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="330"/>
        <location filename="../../src/generated/ui_main_window.h" line="756"/>
        <source>Ctrl+Shift+S</source>
        <translation>Ctrl+Maiusc+S</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="340"/>
        <location filename="../../src/generated/ui_main_window.h" line="758"/>
        <source>&amp;Print...</source>
        <translation>Stam&amp;pa...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="350"/>
        <location filename="../../src/generated/ui_main_window.h" line="759"/>
        <source>Pa&amp;ge Setup...</source>
        <translation>Imposta Pa&amp;gina...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="370"/>
        <location filename="../../src/generated/ui_main_window.h" line="761"/>
        <source>&amp;Close</source>
        <translation>&amp;Chiudi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="375"/>
        <location filename="../../src/generated/ui_main_window.h" line="762"/>
        <source>Close All</source>
        <translation>Chiudi Tutti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="378"/>
        <location filename="../../src/generated/ui_main_window.h" line="764"/>
        <source>Ctrl+Shift+W</source>
        <translation>Ctrl+Maiusc+W</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="388"/>
        <location filename="../../src/generated/ui_main_window.h" line="766"/>
        <source>&amp;Exit</source>
        <translation>&amp;Esci</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="398"/>
        <location filename="../../src/generated/ui_main_window.h" line="767"/>
        <source>&amp;Copy</source>
        <translation>&amp;Copina</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="408"/>
        <location filename="../../src/generated/ui_main_window.h" line="768"/>
        <source>&amp;Paste</source>
        <translation>&amp;Incolla</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="418"/>
        <location filename="../../src/generated/ui_main_window.h" line="769"/>
        <source>Cu&amp;t</source>
        <translation>&amp;Taglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="428"/>
        <location filename="../../src/generated/ui_main_window.h" line="770"/>
        <source>Select &amp;All</source>
        <translation>Selezion&amp;a Tutto</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="438"/>
        <location filename="../../src/generated/ui_main_window.h" line="771"/>
        <source>&amp;Preferences...</source>
        <translation>O&amp;pzioni...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="448"/>
        <location filename="../../src/generated/ui_main_window.h" line="772"/>
        <source>&amp;Reset View</source>
        <translation>&amp;Reimposta Vista</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="458"/>
        <location filename="../../src/generated/ui_main_window.h" line="773"/>
        <source>Zoom &amp;In</source>
        <translation>&amp;Ingrandisci</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="468"/>
        <location filename="../../src/generated/ui_main_window.h" line="774"/>
        <source>Zoom &amp;Out</source>
        <translation>&amp;Rimpicciolisci</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="478"/>
        <location filename="../../src/generated/ui_main_window.h" line="775"/>
        <source>&amp;Reset Zoom</source>
        <translation>&amp;Reimposta Zoom</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="481"/>
        <location filename="../../src/generated/ui_main_window.h" line="777"/>
        <source>Ctrl+0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="497"/>
        <location filename="../../src/generated/ui_main_window.h" line="779"/>
        <source>Display &amp;Graph</source>
        <translation>Mostra &amp;Grafo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="611"/>
        <location filename="../../src/generated/ui_main_window.h" line="797"/>
        <source>&amp;Select</source>
        <translation>&amp;Selezione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="627"/>
        <location filename="../../src/generated/ui_main_window.h" line="801"/>
        <source>Edge &amp;Chain</source>
        <translation>&amp;Catena di archi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="723"/>
        <location filename="../../src/generated/ui_main_window.h" line="816"/>
        <source>Report &amp;Bugs...</source>
        <translation>Segnala Pro&amp;blemi...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="770"/>
        <location filename="../../src/generated/ui_main_window.h" line="823"/>
        <source>&amp;Fit View</source>
        <translation>A&amp;datta visualizzazione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="773"/>
        <location filename="../../src/generated/ui_main_window.h" line="825"/>
        <source>Scroll and zoom the view to display the entire knot</source>
        <translation>Muove e ridimensiona la visyalizzazione in modo da mostrare l&apos;intreccio completo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="112"/>
        <location filename="../../src/generated/ui_main_window.h" line="832"/>
        <source>&amp;Dialogs</source>
        <translation>&amp;Dialoghi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="189"/>
        <location filename="../../src/generated/ui_main_window.h" line="838"/>
        <source>&amp;Grid</source>
        <translation>&amp;Griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="360"/>
        <location filename="../../src/generated/ui_main_window.h" line="760"/>
        <source>Print Pre&amp;view...</source>
        <translation>Antepri&amp;ma di Stampa...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="507"/>
        <location filename="../../src/generated/ui_main_window.h" line="780"/>
        <source>&amp;Connect</source>
        <translation>&amp;Connetti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="510"/>
        <location filename="../../src/generated/ui_main_window.h" line="782"/>
        <source>F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="520"/>
        <location filename="../../src/generated/ui_main_window.h" line="784"/>
        <source>&amp;Disconnect</source>
        <translation>&amp;Disconnetti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="530"/>
        <location filename="../../src/generated/ui_main_window.h" line="785"/>
        <source>&amp;Horizontal Flip</source>
        <translation>Capovolgi &amp;Orizzontalmente</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="540"/>
        <location filename="../../src/generated/ui_main_window.h" line="786"/>
        <source>&amp;Vertical Flip</source>
        <translation>Capovolgi &amp;Verticalmente</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="553"/>
        <location filename="../../src/generated/ui_main_window.h" line="787"/>
        <source>&amp;Rotate</source>
        <translation>&amp;Ruota</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="569"/>
        <location filename="../../src/generated/ui_main_window.h" line="788"/>
        <source>&amp;Scale</source>
        <translation>Ridimen&amp;siona</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="579"/>
        <location filename="../../src/generated/ui_main_window.h" line="789"/>
        <source>&amp;Merge</source>
        <translation>&amp;Unisci</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="582"/>
        <location filename="../../src/generated/ui_main_window.h" line="791"/>
        <source>Ctrl+M</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="592"/>
        <location filename="../../src/generated/ui_main_window.h" line="793"/>
        <source>&amp;Erase</source>
        <translation>&amp;Rimuovi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="595"/>
        <location filename="../../src/generated/ui_main_window.h" line="795"/>
        <source>Del</source>
        <translation>Canc</translation>
    </message>
    <message>
        <source>&amp;Edit Graph</source>
        <translation type="obsolete">&amp;Modifica Grafo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="614"/>
        <location filename="../../src/generated/ui_main_window.h" line="799"/>
        <source>Alt+Shift+E</source>
        <translation>Alt+Maiusc+E</translation>
    </message>
    <message>
        <source>Edge &amp;Loop</source>
        <translation type="obsolete">&amp;Lista di Archi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="630"/>
        <location filename="../../src/generated/ui_main_window.h" line="803"/>
        <source>Alt+Shift+L</source>
        <translation>Alt+Maiusc+L</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="760"/>
        <location filename="../../src/generated/ui_main_window.h" line="822"/>
        <source>&amp;Toggle Edges</source>
        <translation>Abili&amp;ta/Disabilita Archi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="640"/>
        <location filename="../../src/generated/ui_main_window.h" line="805"/>
        <source>&amp;Refresh Path</source>
        <translation>&amp;Aggiorna Immagine</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="650"/>
        <location filename="../../src/generated/ui_main_window.h" line="806"/>
        <source>&amp;Manual</source>
        <translation>&amp;Manuale</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="660"/>
        <location filename="../../src/generated/ui_main_window.h" line="807"/>
        <source>&amp;About Knotter...</source>
        <translation>Inform&amp;azioni su Knotter...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="728"/>
        <location filename="../../src/generated/ui_main_window.h" line="817"/>
        <source>Select Connected</source>
        <translation>Seleziona connessi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="731"/>
        <location filename="../../src/generated/ui_main_window.h" line="819"/>
        <source>Ctrl+L</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="747"/>
        <location filename="../../src/generated/ui_main_window.h" line="821"/>
        <source>&amp;Enable Grid</source>
        <translation>A&amp;bilita Griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="670"/>
        <location filename="../../src/generated/ui_main_window.h" line="808"/>
        <source>E&amp;xport...</source>
        <translation>Es&amp;porta...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="673"/>
        <location filename="../../src/generated/ui_main_window.h" line="810"/>
        <source>Ctrl+E</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="683"/>
        <location filename="../../src/generated/ui_main_window.h" line="812"/>
        <source>Snap to &amp;Grid</source>
        <translation>Allinea alla &amp;Griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="693"/>
        <location filename="../../src/generated/ui_main_window.h" line="813"/>
        <source>&amp;Undo</source>
        <translation>&amp;Annulla</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="703"/>
        <location filename="../../src/generated/ui_main_window.h" line="814"/>
        <source>&amp;Redo</source>
        <translation>&amp;Ripeti</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.ui" line="713"/>
        <location filename="../../src/generated/ui_main_window.h" line="815"/>
        <source>&amp;Configure Plugins...</source>
        <translation>&amp;Configura Estensioni...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="182"/>
        <source>Zoom</source>
        <translation></translation>
    </message>
    <message>
        <source>Knot Style</source>
        <translation type="obsolete">Sile Nodo</translation>
    </message>
    <message>
        <source>Selection Style</source>
        <translation type="obsolete">Sile Selezione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="281"/>
        <source>Action History</source>
        <translation>Storico Azioni</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="285"/>
        <source>Selected Nodes</source>
        <translation>Nodi selezionati</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="288"/>
        <source>Selected Edges</source>
        <translation>Archi selezionati</translation>
    </message>
    <message>
        <source>Default Style</source>
        <translation type="obsolete">Sile predefinito</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>W</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="161"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="162"/>
        <source>Viewport X position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="162"/>
        <source>Viewport Y position</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="163"/>
        <source>Viewport width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="163"/>
        <source>Viewport height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="302"/>
        <source>Warning:</source>
        <translation>Attenzione:</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="303"/>
        <source>Discarding old configuration</source>
        <translation>La vecchia configurazione verrà ignorara</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="309"/>
        <source>Load old configuration</source>
        <translation>Carica vecchia configurazione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="310"/>
        <source>Knotter has detected configuration for version %1,
this is version %2.
Do you want to load it anyways?</source>
        <translation>Knotter ha identificato delle impostazioni per la versione %1,
Questa è la versione %2.
Vuoi caricare le vecchie impostazioni?</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="538"/>
        <location filename="../../src/dialogs/main_window.cpp" line="674"/>
        <source>New Knot</source>
        <translation>Nuovo Nodo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="545"/>
        <source>%1 - %2%3</source>
        <extracomment>Main window title * %1 is the program name * %2 is the file name * %3 is a star * or an empty string depending on whether the file was modified</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="682"/>
        <location filename="../../src/dialogs/main_window.cpp" line="847"/>
        <source>File Error</source>
        <translation>Errore di file</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="683"/>
        <source>Error while reading &quot;%1&quot;.</source>
        <translation>Errore durante la lettura di &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="718"/>
        <source>Close File</source>
        <translation>Chiudi File</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="719"/>
        <source>The file &quot;%1&quot; has been modified.
Do you want to save changes?</source>
        <translation>Il file &quot;%1&quot; è stato modificato.
Vuoi salvare le modifiche?</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="750"/>
        <source>Undo %1</source>
        <translation>Annlla %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="755"/>
        <source>Redo %1</source>
        <translation>Ripristina %1</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="797"/>
        <source>Open Knot</source>
        <translation>Apri nodo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="827"/>
        <source>Knot files (*.knot);;XML files (*.xml);;All files (*)</source>
        <translation>File intreccio (*.knot);;File XML (*.knot);;Tutti i file(*)</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="828"/>
        <source>Save Knot</source>
        <translation>Salva nodo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="848"/>
        <source>Failed to save file &quot;%1&quot;.</source>
        <translation>Errore durante il salvataggio di &quot;%1&quot;.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="855"/>
        <source>(%1,%2)</source>
        <extracomment>Displaying mouse position, %1 = x, %2 = y</extracomment>
        <translation></translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="876"/>
        <source>No recent files</source>
        <translation>Nessun file recente</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1031"/>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1043"/>
        <source>Cut</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1094"/>
        <source>Drop</source>
        <translation>Nodo Rilasciato</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1113"/>
        <source>Snap to Grid</source>
        <translation>Blocca sulla griglia</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1128"/>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1193"/>
        <source>Connect Nodes</source>
        <translation>Connetti vertici</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1212"/>
        <source>Disconnect Nodes</source>
        <translation>Disconnetti vertici</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/main_window.cpp" line="1230"/>
        <source>Merge Nodes</source>
        <translation>Unisci vertici</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="14"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="21"/>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="28"/>
        <source>Clear</source>
        <translation type="unfinished">Cancella</translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="58"/>
        <source>&amp;Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="67"/>
        <source>&amp;Action1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="70"/>
        <source>Ctrl+1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="75"/>
        <source>A&amp;ction2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/keysequence_widget/keysequence_widget_demo/main_window.ui" line="78"/>
        <source>Alt+2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Move_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="312"/>
        <source>Move Node</source>
        <translation>Sposta vertice</translation>
    </message>
</context>
<context>
    <name>Node_Style_All</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="700"/>
        <source>Change Node Style</source>
        <translation>Cambia stile vertice</translation>
    </message>
</context>
<context>
    <name>Node_Style_Crossing_Distance</name>
    <message>
        <source>Change Selection Crossing Gap</source>
        <translation type="obsolete">Cambia spazio d&apos;incrocio della selezione</translation>
    </message>
</context>
<context>
    <name>Node_Style_Cusp_Angle</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="547"/>
        <source>Change Selection Cusp Angle</source>
        <translation>Cambia angolo minimo per cuspide della selezione</translation>
    </message>
</context>
<context>
    <name>Node_Style_Cusp_Distance</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="530"/>
        <source>Change Selection Cusp Distance</source>
        <translation>Cambia distanza cuspide della selezione</translation>
    </message>
</context>
<context>
    <name>Node_Style_Cusp_Shape</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="620"/>
        <source>Change Selection Cusp Shape</source>
        <translation>Cambia forma cuspide della selezione</translation>
    </message>
</context>
<context>
    <name>Node_Style_Enable</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="642"/>
        <source>Toggle Selection Style Property</source>
        <translation>Cambia proprietà di stile</translation>
    </message>
</context>
<context>
    <name>Node_Style_Handle_Lenght</name>
    <message>
        <source>Change Selection Curve Control</source>
        <translation type="obsolete">Cambia controllo curve della selezione</translation>
    </message>
    <message>
        <location filename="../../src/widgets/knot_view/commands.hpp" line="513"/>
        <source>Change Node Curve Control</source>
        <translation>Cambia controllo curve del nodo</translation>
    </message>
</context>
<context>
    <name>Pen_Join_Style</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="410"/>
        <source>Change Joint Style</source>
        <translation>Cambia sile punte</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/command_line.cpp" line="83"/>
        <location filename="../../src/command_line.cpp" line="86"/>
        <location filename="../../src/string_toolbar.cpp" line="62"/>
        <source>Warning:</source>
        <translation>Attenzione:</translation>
    </message>
    <message>
        <location filename="../../src/command_line.cpp" line="84"/>
        <source>Missing file for argument %1</source>
        <translation>Manca il file per l&apos;opzione %1</translation>
    </message>
    <message>
        <location filename="../../src/command_line.cpp" line="87"/>
        <source>No input file specified for option %1</source>
        <translation>Nessun file specificato per l&apos;opzione %1</translation>
    </message>
    <message>
        <location filename="../../src/string_toolbar.cpp" line="63"/>
        <source>Unknown action %1</source>
        <translation>Azione %1 sconosciuta</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="111"/>
        <source>Regular</source>
        <translation>Normale</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="128"/>
        <source>Inverted</source>
        <translation>Invertito</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="212"/>
        <source>Wall</source>
        <translation>Muro</translation>
    </message>
    <message>
        <location filename="../../src/graph/edge_type.cpp" line="266"/>
        <source>Hole</source>
        <translation>Buco</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="90"/>
        <source>Round</source>
        <translation>Curvilinea</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="104"/>
        <source>Pointed</source>
        <translation>Appuntito</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="118"/>
        <source>Ogee</source>
        <translation>Ogivale</translation>
    </message>
    <message>
        <location filename="../../src/graph/node_cusp_shape.hpp" line="132"/>
        <source>Polygonal</source>
        <translation>Poligonale</translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="291"/>
        <source>%1:%2:Error: %3</source>
        <translation>%1:%2:Errore:%3</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="144"/>
        <source>Cusp</source>
        <translation>Cuspide</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="145"/>
        <source>Crossing</source>
        <translation>Incrocio</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="146"/>
        <source>Other</source>
        <translation>Altro</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="147"/>
        <source>Invalid</source>
        <translation>Invalido</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="165"/>
        <source>Unknown plugin type</source>
        <translation>Tipo del plugin non riconosciuto</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="172"/>
        <source>Missing script file</source>
        <translation>Manca file di script</translation>
    </message>
    <message>
        <location filename="../../src/scripting/plugin.cpp" line="178"/>
        <source>Error while opening script file %1</source>
        <translation>Errore durante l&apos;apertura del file di script %1</translation>
    </message>
    <message>
        <location filename="../../src/scripting/misc_script_functions.cpp" line="54"/>
        <source>Expected file name for run_script()</source>
        <translation>Nome di un file previsto da run_script()</translation>
    </message>
    <message>
        <location filename="../../src/scripting/misc_script_functions.cpp" line="57"/>
        <source>Cannot open file &quot;%1&quot;</source>
        <translation>Impossibile aprire il file %1</translation>
    </message>
</context>
<context>
    <name>Remove_Edge</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="159"/>
        <source>Remove Edge</source>
        <translation>Rimuovi arco</translation>
    </message>
</context>
<context>
    <name>Remove_Node</name>
    <message>
        <location filename="../../src/widgets/knot_view/commands.cpp" line="383"/>
        <source>Remove Node</source>
        <translation>Rimuovi vertice</translation>
    </message>
</context>
<context>
    <name>Resource_Manager</name>
    <message>
        <location filename="../../src/resource_manager.cpp" line="82"/>
        <location filename="../../src/resource_manager.cpp" line="150"/>
        <location filename="../../src/resource_manager.cpp" line="201"/>
        <source>Warning:</source>
        <translation>Attenzione:</translation>
    </message>
    <message>
        <location filename="../../src/resource_manager.cpp" line="83"/>
        <source>Unrecognised translation file name pattern: %1</source>
        <translation>Nome file di traduzione non riconosciuto: %1</translation>
    </message>
    <message>
        <location filename="../../src/resource_manager.cpp" line="155"/>
        <source>Error on loading translation file %1 for language %2 (%3)</source>
        <translation>Errore durante il caricamento del file di traduzione %1 per la lingua %2 (%3)</translation>
    </message>
    <message>
        <location filename="../../src/resource_manager.cpp" line="202"/>
        <source>There is no translation for language %1 (%2)</source>
        <translation>Nessun file di traduzione per la lingua %1 (%2)</translation>
    </message>
    <message>
        <source>Cannot open file</source>
        <translation type="vanished">Impossibile aprire il file</translation>
    </message>
    <message>
        <source>Unknown error</source>
        <translation type="vanished">Errore sconosciuto</translation>
    </message>
    <message>
        <source>%1: Error: %2</source>
        <translation type="vanished">%1: Errore: %2</translation>
    </message>
    <message>
        <source>Script aborted</source>
        <translation type="vanished">Script terminato di forza</translation>
    </message>
    <message>
        <source>Script timeout</source>
        <translation type="obsolete">Esecuzione script troppo lunga</translation>
    </message>
</context>
<context>
    <name>Resource_Script</name>
    <message>
        <location filename="../../src/resource_script.cpp" line="257"/>
        <source>Cannot open file</source>
        <translation type="unfinished">Impossibile aprire il file</translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="322"/>
        <source>Unknown error</source>
        <translation type="unfinished">Errore sconosciuto</translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="325"/>
        <source>%1: Error: %2</source>
        <translation type="unfinished">%1: Errore: %2</translation>
    </message>
    <message>
        <location filename="../../src/resource_script.cpp" line="359"/>
        <source>Script aborted</source>
        <translation type="unfinished">Script terminato di forza</translation>
    </message>
</context>
<context>
    <name>Script_Document</name>
    <message>
        <location filename="../../src/scripting/wrappers/script_document.cpp" line="77"/>
        <source>Script Insert</source>
        <translation>Inerimento da script</translation>
    </message>
    <message>
        <location filename="../../src/scripting/wrappers/script_document.cpp" line="160"/>
        <source>Load File</source>
        <translation>Carica file</translation>
    </message>
</context>
<context>
    <name>Script_QTableWidget</name>
    <message>
        <location filename="../../src/scripting/wrappers/script_qtablewidget.cpp" line="88"/>
        <source>Argument is not a QTableWidget object</source>
        <translation>L&apos;argomento non è un oggetto di QTableWidget</translation>
    </message>
</context>
<context>
    <name>Script_Window_Dialog</name>
    <message>
        <location filename="../../src/scripting/wrappers/script_window.cpp" line="212"/>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../../src/settings.cpp" line="114"/>
        <source>Warning:</source>
        <translation>Attenzione:</translation>
    </message>
    <message>
        <location filename="../../src/settings.cpp" line="115"/>
        <source>Not loading toolbar without name</source>
        <translation>Barra degli strumenti senza nome non verrà caricata</translation>
    </message>
</context>
<context>
    <name>Test_Window</name>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="14"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="172"/>
        <source>Test_Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="24"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="180"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="41"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="181"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;File</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="50"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="182"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="54"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="183"/>
        <source>Something</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="79"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="184"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="98"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="173"/>
        <source>New</source>
        <translation type="unfinished">Nuova</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="108"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="174"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="118"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="175"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="128"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="176"/>
        <source>Undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="138"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="177"/>
        <source>Redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="143"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="178"/>
        <source>Do A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/demo/test_window.ui" line="148"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_test_window.h" line="179"/>
        <source>Do B</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Toolbar_Editor</name>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="23"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="189"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="191"/>
        <source>Select a menu</source>
        <translation>Seleziona un menù</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="26"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="192"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="194"/>
        <source>Lists the avaliable menus to get the corresponding actions</source>
        <translation>Lista dei menù disponibili</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="33"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="195"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="197"/>
        <source>Select toolbar</source>
        <translation>Seleziona barra degli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="36"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="198"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="200"/>
        <source>List the available toolbars. The selected one will be edited</source>
        <translation>Lista delle barre degli strumenti. Quella selezionata sarà quella che viene modificata</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="43"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="201"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="203"/>
        <source>Lists the actions in the selected toolbar</source>
        <translation>Lista delle azioni nella barra selezionata</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="52"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="204"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="206"/>
        <source>Add new Toolbar</source>
        <translation>Aggiungi nuova barra degli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="55"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="206"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="208"/>
        <source>New</source>
        <translation>Nuova</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="67"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="208"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="210"/>
        <source>Delete selected toolbar</source>
        <translation>Cancella la barra degli strumenti selezionata</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="70"/>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="173"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="210"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="236"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="212"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="238"/>
        <source>Remove</source>
        <translation>Rimuovi</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="84"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="212"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="214"/>
        <source>Lists the available actions for the selected menu</source>
        <translation>Lista delle azioni per il menù selezionato</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="106"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="215"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="217"/>
        <source>Move Up</source>
        <translation>Muovi Su</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="109"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="218"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="220"/>
        <source>Moves the selected toolbar item up</source>
        <translation>Muovi l&apos;elemento verso l&apos;alto</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="112"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="220"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="222"/>
        <source>Move &amp;Up</source>
        <translation>Muovi S&amp;u</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="124"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="222"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="224"/>
        <source>Move Down</source>
        <translation>Muovi Giù</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="127"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="225"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="227"/>
        <source>moves the selected toolbar item down</source>
        <translation>Muove l&apos;elemento verso il basso</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="130"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="227"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="229"/>
        <source>Move &amp;Down</source>
        <translation>Muovi &amp;Giù</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="155"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="229"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="231"/>
        <source>Insert</source>
        <translation>Inserisci</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="158"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="232"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="234"/>
        <source>Adds the selected menu action to the toolbar</source>
        <translation>Aggiungi elemento</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="161"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="234"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="236"/>
        <source>&amp;Insert</source>
        <translation>&amp;Inserisci</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="176"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="239"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="241"/>
        <source>Removes the selected item from the active toolbar</source>
        <translation>Rimuovi l&apos;elemento selezionato dalla barra segli strumenti</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="179"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="241"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="243"/>
        <source>&amp;Remove</source>
        <translation>&amp;Rimuovi</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="204"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="243"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="245"/>
        <source>Separator</source>
        <translation>Separatore</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.ui" line="207"/>
        <location filename="../../src/generated/ui_toolbar_editor.h" line="245"/>
        <location filename="../../src/widgets/toolbar_editor/out/generated/ui_toolbar_editor.h" line="247"/>
        <source>&amp;Separator</source>
        <translation>&amp;Separatore</translation>
    </message>
    <message>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.cpp" line="155"/>
        <location filename="../../src/widgets/toolbar_editor/src/toolbar_editor.cpp" line="175"/>
        <source>--(separator)--</source>
        <translation>--(separatore)--</translation>
    </message>
    <message>
        <source>custom_toolbar_%1</source>
        <translation type="vanished">barra_personalizzata_%1</translation>
    </message>
</context>
<context>
    <name>Wizard_Create_Plugin</name>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="14"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="186"/>
        <source>Create Plugin Wizard</source>
        <translation>Wizard di creazione plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="23"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="187"/>
        <source>Plugin Information</source>
        <translation>Informazioni plugin</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="29"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="188"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="39"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="189"/>
        <source>Type</source>
        <translation>Tipo</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="46"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="190"/>
        <source>Description</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="60"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="191"/>
        <source>Script Source</source>
        <translation>Sorgente Script</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="70"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="192"/>
        <source>Additional Data</source>
        <translation>Dati aggiuntivi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="76"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="193"/>
        <source>&amp;Remove</source>
        <translation>&amp;Rimuovi</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="91"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="194"/>
        <source>&amp;Insert</source>
        <translation>&amp;Inserisci</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="116"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="196"/>
        <source>Key</source>
        <translation>Chiave</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="121"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="198"/>
        <source>Value</source>
        <translation>Valore</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="130"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="199"/>
        <source>Output Directory</source>
        <translation>Cartella di destinazione</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.ui" line="139"/>
        <location filename="../../src/generated/ui_wizard_create_plugin.h" line="200"/>
        <source>Browse...</source>
        <translation>Sfoglia...</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="40"/>
        <source>Script</source>
        <translation>Script</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="41"/>
        <source>Cusp</source>
        <translation>Cuspide</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="42"/>
        <source>Crossing</source>
        <translation>Incrocio</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="73"/>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="84"/>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="115"/>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="137"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="73"/>
        <source>Name is required</source>
        <translation>Il nome è obbligatorio</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="85"/>
        <source>%1 is not a writable directory.</source>
        <translation>%1 non è una cartella scrivibile.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="115"/>
        <source>Error while creating plugin file.</source>
        <translation>Errore durante la creazione del file del plugin.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="137"/>
        <source>Error while creating source file.</source>
        <translation>Errore durante la crezione del file sorgente.</translation>
    </message>
    <message>
        <location filename="../../src/dialogs/wizard_create_plugin.cpp" line="158"/>
        <source>Select Directory</source>
        <translation>Seleziona cartella</translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorDialog</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_dialog.cpp" line="59"/>
        <source>Pick</source>
        <translation type="unfinished">Seleziona</translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorPalette</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette.cpp" line="428"/>
        <source>Unnamed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorPaletteModel</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_model.cpp" line="70"/>
        <source>Unnamed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_model.cpp" line="144"/>
        <source>%1 (%2 colors)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::ColorPaletteWidget</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="59"/>
        <source>Open a new palette from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="71"/>
        <source>Create a new palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="83"/>
        <source>Duplicate the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="121"/>
        <source>Delete the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="133"/>
        <source>Revert changes to the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="145"/>
        <source>Save changes to the current palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="170"/>
        <source>Add a color to the palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.ui" line="182"/>
        <source>Remove the selected color from the palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="186"/>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="201"/>
        <source>New Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="187"/>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="202"/>
        <source>Name</source>
        <translation type="unfinished">Nome</translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="228"/>
        <source>GIMP Palettes (*.gpl)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="229"/>
        <source>Palette Image (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="230"/>
        <source>All Files (*)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="231"/>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="244"/>
        <source>Open Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/color_palette_widget.cpp" line="245"/>
        <source>Failed to load the palette file
%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::GradientEditor</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_editor.cpp" line="335"/>
        <source>Add Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_editor.cpp" line="344"/>
        <source>Remove Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_editor.cpp" line="352"/>
        <source>Edit Color...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::GradientListModel</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/gradient_list_model.cpp" line="231"/>
        <source>%1 (%2 colors)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>color_widgets::Swatch</name>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/swatch.cpp" line="824"/>
        <source>Clear Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widgets/color/src/QtColorWidgets/swatch.cpp" line="833"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
